package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

/**
 * classe concrète de déserialisation
 */
public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFileFrame> {

    /**
     * Constructeur de classe
     */
    OutputStream imageStream ;
    public ImageDeserializerBase64DatabaseImpl(OutputStream imageStream) {
        this.imageStream=imageStream;
    }


    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(ResumableImageFileFrame media) throws IOException {
        // récupération du flux ou de la base de données de badges
        BufferedReader br = media.getEncodedImageReader(true);
        // lecture de la base par ligne
        String[] data = br.readLine().split(";");
        // création de l'objet sur lequel sera écrit le flux à décoder
        try (OutputStream os = getSourceOutputStream()) {
            // envoi en déserialisation de la ligne lue
            getDeserializingStream(data[2]).transferTo(os);
        }
    }


    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @return
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return imageStream ;
    }

    /**
     * Setter pour un objet outputstream lors d'une désérialisation
     *
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.imageStream=os;
    }

    /**
     * Décodage pour la restituion d'un badge
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}
