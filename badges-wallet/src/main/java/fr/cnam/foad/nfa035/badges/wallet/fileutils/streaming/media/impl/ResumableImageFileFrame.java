package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;

import java.io.*;

/**
 * Implémentation d'ImageFrame pour un simple Fichier comme canal
 */
public class ResumableImageFileFrame extends AbstractImageFrameMedia<File> implements ResumableImageFrameMedia<File> {

    private BufferedReader reader;

    public ResumableImageFileFrame(File walletDatabase) {
        super(walletDatabase);
    }

    /**
     * Récupération de la base de données d'écriture de badge
     */
    @Override
    public OutputStream getEncodedImageOutput() throws FileNotFoundException {
        return new FileOutputStream(getChannel(), true);
    }

    /**
     * Récupération de la base de données de badge en vue de restituer les badges
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new FileInputStream(getChannel());
    }


    /**
     * Singleton de base de données de badges
     */
    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (reader == null || !resume){
            this.reader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel())));
        }
        return this.reader;
    }

}
