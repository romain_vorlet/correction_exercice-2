package fr.cnam.foad.nfa035.badges.wallet.dao;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface DAO de création et de récupération de badge
 */
public interface BadgeWalletDAO {

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    void addBadge(File image) throws IOException;

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    void getBadge(OutputStream imageStream) throws IOException;
}
