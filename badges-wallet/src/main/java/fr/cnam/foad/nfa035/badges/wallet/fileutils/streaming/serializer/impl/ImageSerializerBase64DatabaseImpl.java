package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;

/**
 * Classe concrète d'ajout de badge encodé dans la base de données
 * Permet la gestion multibadges, en ajoutant un bagde une ligne après l'autre après l'avoir identifié par un ID et sa taille en octet
 */
public class ImageSerializerBase64DatabaseImpl
        extends AbstractStreamingImageSerializer<File, ImageFrameMedia>{

    /**
     * Méthode de sérialisation et d'encodage qui prend un fichier ou un flux pour l'écriture dans une base de données
     * @param source
     * @param media
     * @throws IOException
     */
    public void serialize(File source, ImageFrameMedia media) throws IOException {
        long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()) {
            Writer writer = new OutputStreamWriter(os);
            StringBuilder sb = new StringBuilder();
            sb.append(numberOfLines+1);
            sb.append(';');
            sb.append(size);
            sb.append(';');
            writer.write(sb.toString());
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);

                writer.write("\n");
            }
            writer.flush();
        }
    }

    /**
     * Utile pour récupérer un Flux de lecture de la source à sérialiser
     *
     * @param source
     * @return
     * @throws IOException
     */
    @Override
    public <K extends InputStream> K getSourceInputStream(File source) throws IOException {
        return (K) new FileInputStream(source);
    }

    /**
     * Permet de récupérer le flux d'écriture et de sérialisation vers le media
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public <T extends OutputStream> T getSerializingStream(ImageFrameMedia media) throws IOException {
        return (T) new Base64OutputStream (media.getEncodedImageOutput(),true, -1,null);

    }

}
