package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;


public interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T>  {
    /**
     * Charge le fichier ou le flux dans le buffer
     */
    BufferedReader getEncodedImageReader(boolean resume) throws IOException ;

}
