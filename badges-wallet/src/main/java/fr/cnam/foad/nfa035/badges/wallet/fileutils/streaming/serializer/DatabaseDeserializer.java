package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;

/**
 * Inteface de récupération du flux ou de la base de données à restituer
 * @param <M>
 */

public interface DatabaseDeserializer<M extends ImageFrameMedia> extends BadgeDeserializer<M> {
    <K extends InputStream> K getDeserializingStream(String data) throws IOException;
}
