package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface d'écriture des badges désérialisés
 * @param <M>
 */

public interface BadgeDeserializer <M extends ImageFrameMedia> {

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     * @param media
     * @throws IOException
     */
    void deserialize(M media) throws IOException;

    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * Setter pour un objet outputstream lors d'une désérialisation
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);

}

