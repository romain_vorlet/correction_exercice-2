package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ResumableImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Classe concrète de gestion de badges : ajout et récupération de badges dans une base données
 */

public class MultiBadgeWalletDAOImpl implements BadgeWalletDAO {

    private File walletDatabase;
    private ResumableImageFrameMedia media;

    /**
     * Constructeur de classe
     *
     * @param dbPath
     * @throws IOException
     */
    public MultiBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
        this.media = new ResumableImageFileFrame(walletDatabase);
    }


    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException{
        ImageStreamingSerializer serializer = new ImageSerializerBase64DatabaseImpl();
        serializer.serialize(image, media);
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */

    public void getBadge(OutputStream imageStream) throws IOException{
       DatabaseDeserializer deserializer = new ImageDeserializerBase64DatabaseImpl(imageStream);
       deserializer.deserialize(media);
    }

}

